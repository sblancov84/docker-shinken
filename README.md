# Shinken in Docker!

Just that. No more no less.

This project is done with the aim to know how to install, configure and use
Shinken + Thruk.

This configuration is not recommended to be used in a production environment,
but it works!


## Installed modules

* livestatus
* logstore_null
* logstore_sqlite
* logstore_mongodb

## Modules to install

* [npcdmod](https://github.com/shinken-monitoring/mod-npcdmod)
* [influxdb](https://github.com/savoirfairelinux/mod-influxdb)
* [retention-file-scheduler](https://github.com/shinken-monitoring/mod-pickle-retention-file-scheduler)
