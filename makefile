build-shinken:
	docker build --pull --rm -f shinken/Dockerfile -t shinken:latest "shinken"

build-thruk:
	docker build --pull --rm -f thruk/Dockerfile -t thruk:latest "thruk"

remove-shinken:
	docker image rm shinken:latest

start:
	docker-compose -f docker-compose.yml up -d

stop:
	docker-compose -f docker-compose.yml down
